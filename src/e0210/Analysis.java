package e0210;
package soot.jimple;

/*
 * @author Sridhar Gopinath		-		g.sridhar53@gmail.com
 * 
 * Course project,
 * Principles of Programming Course, Fall - 2016,
 * Computer Science and Automation (CSA),
 * Indian Institute of Science (IISc),
 * Bangalore
 */

import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.Main;

public class Analysis extends BodyTransformer {

	@Override
	protected void internalTransform(Body b, String phaseName, Map<String, String> options) {
		
		float i = soot.Main.project.length;
		int counter=0;
		counter=Local.func1(float i);
		System.out.println(b.toString());

		return;
	}

}


	public interface Local Immediate
	{
	
		int func1(float i)
	{
		void setName(String counter1);
		void setType(int counter1);
		counter1=0;
		while(i!=0)
	{
		if(i%2==0)
		{
			Jimple.v().newAddExpr(i,IntConstant.v(10));
			counter1++;
		}
		else
		counter1++;
		i--;
		
	}
		System.out.println(counter1);
		return counter1;
	}


}



